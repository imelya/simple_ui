#! /usr/bin/env python3.7
import storage

# изменение расписания
storage.change_schedule(
    1, {"1": "математика", 2: "концерт"}
)
# полное расписание
print(storage.get_schedule())
# расписание на день от 1 до 7
print(storage.get_schedule(1))


# добавить заметки
storage.add_note(1, "концерт", "время проведения 12:00")

# добавить задачи
storage.add_note(1, "математика", "сделать задачу №2", todo=True)

# все заметки
# ели нужны задачи, можно отфильтровать по ключу
#   if is_task is not None
print(storage.get_all_notes())

# завершить задачу
storage.done(1, "математика", 1)

# удалить задачу из хранилище
storage.delete_note(1, "математика", 1)
